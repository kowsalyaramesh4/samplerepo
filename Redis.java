
import redis.clients.jedis.BinaryClient;
import redis.clients.jedis.Jedis;
import java.util.*;
public class Redis {
    public static void main(String args[])
    {
        Jedis jd = new Jedis("localhost");
//        String
        jd.set("name","Kowsalya");
        System.out.println("Value is : " + jd.get("name"));
        System.out.println("SubString : "+ jd.getrange("name",2,5));
        System.out.println("GETbit : "+jd.getbit("name",3));
        System.out.println("Setex : "+jd.setex("Hello",2,"Pavi") );
        System.out.println("Mget : "+jd.mget("name","Hello"));
        jd.set("no","2");
        System.out.println("Incr : "+jd.incr("no"));
        System.out.println("Incrbykey : "+jd.incrBy("no",4));
        System.out.println("Decr :"+jd.decr("no"));
        System.out.println("Append :"+jd.append("name","RGMK"));
        System.out.println("DEl : "+jd.del("no"));

//        Hashes
        jd.hset("msc","name","Kayal");
        System.out.println("Hget : "+jd.hget("msc","name"));
        jd.hset("msc","course","it");
        jd.hsetnx("msc","course","it");
        System.out.println("Hvals : "+jd.hvals("msc"));
        System.out.println("Hmget : "+jd.hmget("msc","name","course"));
        System.out.println("hgetall : "+jd.hgetAll("msc"));
        System.out.println("hdel : "+jd.hdel("msc","course"));

//        List
        jd.lpush("pets","cat","dog");
        jd.lset("pets",1,"rat");
        jd.lrem("pets",1,"dog");
        System.out.println("Lpop : "+jd.lpop("pets"));
        jd.lpushx("pets","elephant");
        jd.rpush("pets","Lion","Horse");
        jd.rpushx("pets","Dianosore");
        jd.rpop("pets");
        System.out.println("Lgetrange : "+jd.lrange("pets",0,10));
        System.out.println("lget : "+jd.lindex("pets",1));
        System.out.println("Ltrim : "+jd.ltrim("pets",0,3));
        System.out.println("Lget : "+jd.llen("pets"));
//        jd.linsert("pets", "after","horse","cows");

//        Sets
        jd.sadd("first","one","two","five","six");
        jd.sadd("last","three","four","one");
        System.out.println("sismem : "+jd.sismember("first","five"));
        System.out.println("srandmem : "+jd.srandmember("first"));
        System.out.println("Scard : "+jd.scard("first"));
        System.out.println("Spop : "+jd.spop("last"));
        System.out.println("sdiff : "+jd.sdiff("last","first"));
        System.out.println("sinter : "+jd.sinter("first","last"));
        System.out.println("Sunion : "+jd.sunion("last","first"));

//        Sorted Sets
        jd.zadd("things1",0,"chair");
        jd.zadd("things1",1,"Pen");
        jd.zadd("things1",0,"Table");
        jd.zadd("things2",1,"Pencil");
        jd.zadd("things2",2,"Book");
        jd.zadd("things2",0,"Pen");
        System.out.println("Zcard : "+jd.zcard("things1"));
        System.out.println("ZrangeByscore : "+jd.zrangeByScore("things1",1,2));
        System.out.println("Zrange : "+jd.zrange("things1",0,2));
        System.out.println("Zscore : "+jd.zscore("things1","Pen"));
        System.out.println("Zrem : "+jd.zrem("things2","Book"));
        System.out.println("Zrevrange : "+jd.zrevrange("things1",0,4));
        System.out.println("Zrevrangebyscore : "+jd.zrevrangeByScore("things1",3,0));
        jd.save();
//        jd.bgsave();

    }
}
